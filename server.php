<?php
session_start();
$conn = new mysqli("127.0.0.1","root","Csc!430iscool","test");
$errors = array();
$email ="";
$password ="";
if($conn->connect_error)
{
	die("Connection failed". $conn->connect_error);
}


//register user
if (isset($_POST['submit'])){
	$email = $_POST['InputEmail'];
	$password = $_POST['InputPassword'];

	$user_check_query = "SELECT * FROM testUsers WHERE email='$email' OR password='$password' LIMIT 1";
	$result = mysqli_query($conn, $user_check_query);
	$user = mysqli_fetch_assoc($result);

	if ($user) { // if user exists
		if ($user['email'] === $email) {
			array_push($errors, "Username already exists");
		}
	}
	

	// Validate password strength
	$uppercase = preg_match('@[A-Z]@', $password);
	$lowercase = preg_match('@[a-z]@', $password);
	$number    = preg_match('@[0-9]@', $password);
	$specialChars = preg_match('@[^\w]@', $password);

	if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
		array_push($errors, 'weak password');
		echo 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
	}

	// Finally, register user if there are no errors in the form
	if (count($errors) == 0) {
		//$passwordEnc = md5($password);//encrypt the password before saving in the database
		$options = [ ' cost' => 15];
		
		$passwordEnc = password_hash($password, PASSWORD_BCRYPT, $options);
		$query = "INSERT INTO testUsers (email, password) 
				  VALUES('$email', '$passwordEnc')";
		mysqli_query($conn, $query);
		$_SESSION['email'] = $email;
		$_SESSION['success'] = "You are now logged in";
		header('location: logPage.php');

	
	}
 
}


if (isset($_POST['login'])) {
  $email = mysqli_real_escape_string($conn, $_POST['InputEmail']);
  $password = mysqli_real_escape_string($conn, $_POST['InputPassword']);
  if (empty($email)) {
  	array_push($errors, "Username is required");
  }
  if (empty($password)) {
  	array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {

        $query = "SELECT * FROM testUsers WHERE email='$email'";  
	$results = mysqli_query($conn, $query);
	$foo = False;  
	if (mysqli_num_rows($results) >0) {
		while($row = mysqli_fetch_array($results)){
		if(password_verify($password , $row['password'])){
			 $_SESSION['email'] = $email;
  		 	 $_SESSION['success'] = "You are now logged in";
  	 		 header('location: dashboard.php');
			$results = NULL;
			$foo = True;
		}
	 }
  	}
	if(!$foo){
  		array_push($errors, "Wrong username/password combination");
  	}
  }
}

if(isset($_POST['upload']))
{



  //echo "test";
  $name = $_FILES['file']['name'];
  $target_dir = "upload/";
  $target_file = $target_dir . basename($_FILES["file"]["name"]);
  // Select file type
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  // Valid file extensions
  $extensions_arr = array("jpg","jpeg","png","gif","png");

  // Check extension
  if( in_array($imageFileType,$extensions_arr) ){
 
	// Insert record
   $query = "insert into testImages(email,name) values('".$_SESSION['email']."','".$name."')";
     mysqli_query($conn,$query);
  
     // Upload file
     move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);

  }


}

 

?>
