<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Register</title>

	<!-- begin boostrap import -->
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
		crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<!-- end boostrap import -->
	
	<link rel="stylesheet" href="GeneralStyle.css">
</head>


<body>
	<div id="navbar" class="topnav">
		<h1>Photo Studio</h1>
	</div>

	<div class="container vertical-center input-form-wrapper"> 		
		<form class="general-form-style" action="" method="post" >
		
			<div class="form-group">
				<label for="InputFirstName">First Name</label>
				<input type="text" class="form-control" id="InputFirstName" required>
			</div>
			
			<div class="form-group">
				<label for="InputLastName">Last Name</label>
				<input type="text" class="form-control" id="InputLastName" required>
			</div>
			
			<div class="form-group">
				<label for="InputEmail">Email address</label>
				<input type="text" class="form-control" name="InputEmail" required>
				<div id="emailTaken"></div>
				<div id="emailInvalid"></div>
			</div>
			
			<div class="form-group">
				<label for="InputPassword">Password</label>
				<input type="password" class="form-control" name="InputPassword" required>
			</div>
			
			<div class="form-group">
				<label for="InputDateOfBirth">Date of Birth</label>
				<input type="date" class="form-control" id="birthDate" required>
				<div id="dateOfBirthError"></div>
			</div>
			<input type="submit" class="btn btn-primary" name="submit" >
		</form>
	</div>

</body>
</html>
