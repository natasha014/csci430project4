<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Photo Studio</title>
	
	<!-- begin boostrap import -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<!-- end boostrap import -->
	<link rel="stylesheet" href="GeneralStyle.css">
	<link rel="stylesheet" href="HomeStyle.css">
	
	

</head>
	
	
<body>
	<div class="container-fluid vertical-center" style="text-align:center;">
		<div class="jumbotron full-width">
			<h1 class="display-4 "><b>Photo Studio</b></h1>
			<p class="lead"><i>Choose an Option</i></p>
			<br><br>
			<p class="lead">
				<a class="btn btn-warning btn-lg" href="logPage.php" role="button">Sign in</a>
				<a class="btn btn-warning btn-lg" href="registerPage.php" role="button">Register</a><br>
			</p>
		</div>
	</div>
	
</body>


</html>
